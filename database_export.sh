#!/bin/bash

if ! grep -q '^vagrant:' /etc/passwd ; then
    echo -e "\033[33mThis file must be executed from within vagrant\033[0m"
else 
    echo "Exporting database to /sql"
    mysqldump -u wordpress -h localhost -pwordpress wordpress > /vagrant/sql/vagrant.sql
fi
