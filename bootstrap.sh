#!/bin/bash

# Provision Vagrant
vagrant up

# Clone the service catalog files
if [ -d "./service_catalog_files" ] ; then
    echo "Skipping download of service catalog files. Files already exist."
else
    echo "Fetching service catalog files:"
    git clone git@bitbucket.org:wfmdigitalweb/service_catalog_files.git ./service_catalog_files
    mv ./service_catalog_files/* ./
    rm -rf service_catalog_files/
fi

# Clone the iWT base theme
if [ -d "./public/wp-content/themes/iwt-wp-framework" ] ; then
    echo "Skipping download of iWT base theme. Files already exist."
else
    echo "Fetching iWT Theme:"
    git clone git@bitbucket.org:wfmdigitalweb/iwt-wp-framework.git ./public/wp-content/themes/iwt-wp-framework
    rm -rf ./public/wp-content/themes/iwt-wp-framework/.git
fi

# Clone the iWT core plugins
if [ -d "./public/wp-content/plugins/README.md" ] ; then
    echo "Skipping download of iWT core plugins. Files already exist."
else
    echo "Fetching iWT core plugins:"
    git clone git@bitbucket.org:wfmdigitalweb/iwt-default-plugins.git ./public/wp-content/core-plugins
    mv ./public/wp-content/core-plugins/* ./public/wp-content/plugins/
    rm -rf ./public/wp-content/core-plugins
fi

# Create the log files
if [ -f "./logs/access.log" ] ; then
    echo "Access log already exists."
else
    echo "Creating access log"
touch ./logs/access.log
fi

if [ -f "./logs/error.log" ] ; then
    echo "Error log already exists."
else
    echo "Creating error log"
touch ./logs/error.log
fi

# Edit wp-config-vagrant constants
echo "Configuring database credentials"
sed -i "" "s/database_name_here/wordpress/g" ./public/wp-config-vagrant.php
sed -i "" "s/username_here/wordpress/g" ./public/wp-config-vagrant.php
sed -i "" "s/password_here/wordpress/g" ./public/wp-config-vagrant.php

# Not ready for this yet
# Create wp-config.php
# mv ./public/wp-config-sample.php ./public/wp-config.php

# Don't know how
# Change database prefix
# sed -i 's/password_here/wordpress/g' ./public/wp-config-vagrant.php

# Can't do this until we have 1 wp-config
# Add salts
# wp config shuffle-salts

# Zip it all up for Service Catalog
zip -r wordpress.zip . -x "public/wp-config.php"
