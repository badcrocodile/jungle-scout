---
###
# Run the setup for a local vagrant installation
# NOTE: All configuration beyond what is required by vagrant alone
#       should go in the generic playbooks.
###
- hosts: all

  tasks:
    ##
    # Package management
    ##
    - name: update repositories
      become: yes
      apt: update_cache=yes upgrade=yes

    - name: install required packages
      become: yes
      apt: name={{ item }} state=present
      with_items:
        - apache2
        - php5
        - php5-curl
        - php5-mysql
        - php5-xdebug
        - php5-mcrypt
        - curl
        - git
        - mysql-server
        - python-mysqldb # Needed on vagrant machine to manage mysql with ansible

    - name: check if restart required
      stat: path=/var/run/reboot-required
      register: reboot_file

    - name: output message if we need a reboot
      debug: msg="REBOOT REQUIRED - vagrant reload"
      when: reboot_file.stat.exists

    ##
    # Apache setup
    ##
    - name: run apache as vagrant user
      become: yes
      replace: dest=/etc/apache2/envvars regexp="=www-data" replace="=vagrant"
      notify: restart apache

    - name: create symlink for virtualhost
      become: yes
      file:
        path:  /etc/apache2/sites-enabled/001-vagrant.conf
        src:   /vagrant/conf/vhost-vagrant.conf
        state: link
      notify: restart apache

    - name: remove 000-default.conf
      become: yes
      file:
        path:  /etc/apache2/sites-enabled/000-default.conf
        state: absent
      notify: restart apache

    - name: enable mod_rewrite
      become: yes
      apache2_module: state=present name=rewrite
      notify: restart apache

    - name: restart apache
      become: yes
      service: name=apache2 state=restarted


    ##
    # MySQL setup
    ##
    - name: destroy database
      become: yes
      mysql_db: name=wordpress state=absent

    - name: create database
      become: yes
      mysql_db: name=wordpress state=present

    - name: setup mysql user
      become: yes
      mysql_user:
        name:     "wordpress"
        password: "wordpress"
        priv:     "wordpress.*:ALL"
        state:    present

    - name: import mysql database
      become: yes
      mysql_db:
         state=import
         target=/vagrant/sql/vagrant.sql
         name=wordpress

    ##
    # Wordpress setup
    ##
    - name: download wordpress latest
      get_url: url=https://wordpress.org/latest.tar.gz dest=/tmp/latest.tar.gz

    - name: extract wordpress
      shell: tar xf /tmp/latest.tar.gz -C /vagrant/public --strip-components=1
      args:
        creates: /vagrant/public/index.php

    - name: create vagrant config file
      command: cp /vagrant/public/wp-config-sample.php /vagrant/public/wp-config-vagrant.php

    - name: create sandbox config file
      command: cp /vagrant/public/wp-config-vagrant.php /vagrant/public/wp-config-sandbox.php

    - name: create int config file
      command: cp /vagrant/public/wp-config-vagrant.php /vagrant/public/wp-config-int.php

    - name: create qa config file
      command: cp /vagrant/public/wp-config-vagrant.php /vagrant/public/wp-config-qa.php

    - name: create prod config file
      command: cp /vagrant/public/wp-config-vagrant.php /vagrant/public/wp-config-prod.php

    - name: link configuration file
      file: state=link src=/vagrant/public/wp-config-vagrant.php dest=/vagrant/public/wp-config.php

    - name: clean up default themes and plugins we don't need
      file: state=absent path=/vagrant/public/{{ item }}
      with_items:
      - wp-content/themes/twentythirteen # Twenty Thirteen Theme
      - wp-content/themes/twentyfourteen # Twenty Fourteen Theme
      - wp-content/themes/twentyfifteen # Twenty Fifteen Theme
      - wp-content/themes/twentyseventeen # Twenty Seventeen Theme
      - wp-content/plugins/hello.php # Hello Dolly plugin
      - wp-content/plugins/akismet.php # Akismet plugin

  handlers:
    - name: restart apache
      become: yes
      service: name=apache2 state=restarted
