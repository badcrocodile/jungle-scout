<?php
/**
 * The template for displaying search results pages
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package wtf
 */

jsd_theme_debug( basename( __DIR__ ), pathinfo( __FILE__, PATHINFO_FILENAME ) );

get_header();

if ( have_posts() ) : ?>
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <header class="entry-header">
                        <h1 class="entry-title"><?php
							/* translators: %s: search query. */
							printf( esc_html__( 'Search Results for: %s', 'wtf' ), '<span>' . get_search_query() . '</span>' );
							?></h1>
						<?php jsd_breadcrumbs() ?>
                    </header><!-- .entry-header -->
                </div>
            </div>
        </div>
    </div>

    <div class="container archive-item">
    <div class="row">
    <div class="col-md-8">
	<?php
	while ( have_posts() ) : the_post();
		/**
		 * Run the loop for the search to output the results.
		 * If you want to overload this in a child theme then include a file
		 * called content-search.php and that will be used instead.
		 */
		get_template_part( 'archives/search', 'default' );
	endwhile;

	the_posts_navigation();

else :
	get_template_part( 'template-parts/content', 'none' );
endif; ?>
    </div>

    <aside id='secondary' class='widget-area col-sm-4'>
		<?php dynamic_sidebar( 'sidebar-post' ) ?>
    </aside>
    </div>
    </div>

<?php get_footer();
