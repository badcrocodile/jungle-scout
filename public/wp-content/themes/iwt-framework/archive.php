<?php
/**
 * The template for displaying archive pages
 */

jsd_theme_debug( basename( __DIR__ ), pathinfo( __FILE__, PATHINFO_FILENAME ) );

get_header();

if ( have_posts() ) : ?>
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <header class="entry-header">
						<?php the_archive_title( '<h1 class="entry-title">', '</h1>' ); ?>
						<?php jsd_breadcrumbs() ?>
                    </header><!-- .entry-header -->
                </div>
            </div>
        </div>
    </div>

    <div class="container archive-item">
    <div class="row">
    <div class="col-md-8">
	<?php
	while ( have_posts() ) : the_post();
		/**
		 * This is the router for archive page requests
		 *
		 * Usage: If you want a different layout for the archive display of Post Type "Books",
		 * add an is_post_type_archive('books) check in the switch below
		 * and point it to your Books template
		 *
		 * page.php holds routing rules for pages
		 * single.php holds routing rules for single posts and post-types
		 */

		switch ( true ) {
			case( is_post_type_archive( 'books' ) ) :
				get_template_part( 'archives/book', 'default' );
				break;
			default :
				get_template_part( 'archives/archive', 'default' );
		}
	endwhile;

else :
	get_template_part( 'template-parts/content', 'none' );
endif;
?>
    </div>

    <aside id='secondary' class='widget-area col-sm-4'>
		<?php dynamic_sidebar( 'sidebar-post' ) ?>
    </aside>
    </div>
    </div>

<?php
get_footer();
