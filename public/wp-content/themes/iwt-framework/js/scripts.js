(function ($) {
    $(document).ready(function () {
        smoothScroll();
        // instantiateSlick();
        mobileNav();
        bootstrapNavHoverClickFix();
        searchOverlay();
        closeSearchOverlay();
        matchHeight();
    });

    function searchOverlay() {
        $('li.search').on('click', function(e) {
            $('.search-backdrop').fadeToggle('fast');
            $('.sf-header-search').fadeToggle('fast');
            $('#s').focus();
        });
    }

    function matchHeight() {
        $('.bundles .bundle').matchHeight();
    }

    function closeSearchOverlay() {
        $('.header-search-close').on('click', function() {
            $('.sf-header-search').fadeOut('fast');
            $('.search-backdrop').fadeOut('fast');
        });

        $('.search-backdrop').on('click', function() {
            $('.sf-header-search').fadeOut('fast');
            $('.search-backdrop').fadeOut('fast');
        })
    }

    /**
     * Bootstrap nav contains default JS to prevent clicking on top-level menu items
     * This is because, natively, click opens the submenu therefore should not activate the link
     * Since we moved to on-hover event to open submenus, we need the click to
     * navigate the user to the desired page. This does that.
     */
    function bootstrapNavHoverClickFix() {
        $('.dropdown-toggle').click(function(e) {
            if ($(document).width() > 768) {
                e.preventDefault();

                var url = $(this).attr('href');


                if (url !== '#') {

                    window.location.href = url;
                }

            }
        });
    }

    /**
     * Instantiate mobile nav and set up a few options
     */
    function mobileNav() {
        var $menu = $("#mobile-navigation").mmenu({
            "extensions": [
                "position-right"
            ],
            "navbars": [
                {
                    "position": "top",
                    "content": [
                        "<form action='/' method='get' class='search' style='padding:0'><input type='text' name='s' id='search' placeholder='Type and hit enter to search' style='width: 100%; height: 41px;' /></form>",
                    ]
                }
            ]
        });

        var API = $menu.data( "mmenu" );
        var $icon = $("#open-mmenu");

        $icon.on( "click", function() {
            API.open();
            API.close();
        });

        $icon.on( "click", function() {
            API.open();
        });

        API.bind( "open:finish", function() {
            setTimeout(function() {
                $icon.addClass( "is-active" );
            }, 100);
        });
        API.bind( "close:finish", function() {
            setTimeout(function() {
                $icon.removeClass( "is-active" );
            }, 100);
        });
    }

    function instantiateSlick() {
        $('.slick').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            dots: true,
            arrows: false
        });
    }

    // Flexslider
    function flex() {
        var id;
        var startSliderAt = getUrlParameter('slide');

        $('.flexslider').flexslider({
            animation: "slide",
            animationSpeed: "1000",
            controlsContainer: $(".custom-controls-container"),
            customDirectionNav: $(".custom-navigation a"),
            animationLoop: true,
            slideshow: false,
            startAt: parseInt(startSliderAt),
            before: function (slider) {
                /**
                 Using query params to allow for deep-linking to specific slides
                 By pushing the ID of the element to the URL as it is displayed
                 */
                id = slider.slides[slider.animatingTo].id;

                history.pushState(null, null, "?slide=" + id);
            },
        });
    }

    // Swipebox
    function swipeBox() {
        $('.swipebox').swipebox({
            autoplayVideos: true,
            hideBarsDelay: 1000000
        });
    }

    // addition for swipebox, closing img on click on bg
    function closeSwipeBox() {
        $(document.body)
            .on('click touchend', '#swipebox-slider .current img', function (e) {
                return false;
            })
            .on('click touchend', '#swipebox-slider .current', function (e) {
                jQuery('#swipebox-close').trigger('click');
            });
    }

    function smoothScroll() {
        $('a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);

                    return false;
                }
            }
        });
    }

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

})(jQuery);
