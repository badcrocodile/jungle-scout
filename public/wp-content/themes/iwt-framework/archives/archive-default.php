<?php
/**
 * Template part for displaying archive content
 */

?>
<?php jsd_theme_debug( basename( __DIR__ ), pathinfo( __FILE__, PATHINFO_FILENAME ) ) ?>

<article data-template="content" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<a href="<?= the_permalink() ?>" title="<?= the_title() ?>" class="card card-type-default">
		<div class="card-content">
			<header class="card-header">
				<h3><?= get_the_title() ?></h3>
			</header><!-- .entry-header -->

			<div class="entry-content">
                <?php if(has_post_thumbnail()) : ?>
                <div class="thumb"><?php the_post_thumbnail('thumb'); ?></div>
                <?php endif ?>
				<?php the_excerpt(); ?>
			</div><!-- .entry-content -->
		</div>
	</a>
</article><!-- #post-## -->
