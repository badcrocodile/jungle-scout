<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 */

jsd_theme_debug( basename( __DIR__ ), pathinfo( __FILE__, PATHINFO_FILENAME ) );


get_header();

while ( have_posts() ) : the_post();
	/**
	 * This is the router for single page requests
	 *
	 * Usage: If you want to add a new layout for a page named 'contact' page add a new
	 * is_page('contact') check in the switch below and point it to your contact.php template
	 *
	 * archive.php holds routing rules for archives
	 * single.php holds routing rules for single posts and post-types
	 */
	switch ( true ) {
		case( is_page( 'home' ) ) :
			get_template_part( 'pages/home');
			break;
		case( is_page( 'contact' ) ) :
			get_template_part( 'pages/contact');
			break;
		default:
			get_template_part( 'pages/default');
	}
endwhile; // End of the loop.

get_footer();
