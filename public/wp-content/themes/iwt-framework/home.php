<?php
/**
 * The template for displaying the blog front page
 */

get_header(); ?>

<?php jsd_theme_debug(pathinfo(__FILE__, PATHINFO_FILENAME), basename(__DIR__)) ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<?php if(have_posts()) : ?>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<header class="page-header">
							<?php
							the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
							?>
						</header><!-- .page-header -->
					</div>
				</div>

				<div class="row">
					<div class="col-sm-9">
						<?php while (have_posts()) : the_post(); ?>
							<?php get_template_part('archives/default', 'archive'); ?>
						<?php endwhile; ?>

						<?php the_posts_navigation(); ?>
					</div>

					<div class="col-sm-3 sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		<?php else : ?>
			<?php get_template_part('template-parts/content', 'none'); ?>
		<?php endif; ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
