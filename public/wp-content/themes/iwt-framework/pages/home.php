<?php
/**
 * Template for displaying home page.
 */
?>

<?php jsd_theme_debug( basename( __DIR__ ), pathinfo( __FILE__, PATHINFO_FILENAME ) ) ?>

<div class="header" style="background-image: url(<?= get_field( 'background_image' ) ?>); background-size: cover; background-position: center; overflow: auto; height: auto">
    <div class="container">
        <div class="row headline">
            <div class="col-sm-6 headline">
                <h1><?= get_field( 'headline' ) ?></h1>
                <p><?= get_field( 'subheading' ) ?></p>
            </div>
            <div class="col-sm-6">
                <div class="pullquote">
                    <div class="quote">"<?= get_field( 'pullquote' )['pullquote_content'] ?>"</div>
                    <div class="quote-author-image"><img src="<?= get_field( 'pullquote' )['pullquote_author_image'] ?>" alt="Image of <?= get_field( 'pullquote' )['pullquote_author'] ?>" title="<?= get_field( 'pullquote' )['pullquote_author'] ?>"></div>
                    <div class="quote-author"><?= get_field( 'pullquote' )['pullquote_author'] ?></div>
                    <div class="quote-author-title"><?= get_field( 'pullquote' )['pullquote_author_title'] ?></div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 reviews">
			<?php while ( have_rows( 'reviews' ) ) : the_row(); ?>
                <div class="review">
                    <div class="review-text">"<?= get_sub_field( 'review' ) ?>"</div>
                    <div class="review-logo"><img src="<?= get_sub_field( 'reviewer_logo' ) ?>" alt="Reviewer Logo" title="Reviewer Logo"></div>
                </div>
			<?php endwhile; ?>
        </div>
    </div>
</div>

<div <?php post_class( "home" ) ?>>
    <section class="container pricing-bundles-wrapper">
        <div class="row intro">
            <div class="col-sm-12">
                <header>
                    <h2><?= get_field( 'pricing_bundles_introduction' ) ?></h2>
                    <p><?= get_field( 'pricing_bundles_introduction_subtext' ) ?></p>
                </header>
            </div>
        </div>
        <div class="row row-eq-height bundles">
			<?php while ( have_rows( 'pricing_bundles' ) ) : the_row(); ?>
                <div class="col-sm-4">
                    <a class="bundle <?= (get_sub_field('bundle_ribbon') ? " ribbon" : "") ?>" href="<?= get_sub_field('cta_button_link') ?>">
                        <div class="bundle-title"><?= get_sub_field( 'bundle_title' ) ?></div>
                        <div class="bundle-value">Valued at $<?= get_sub_field( 'bundle_value' ) ?></div>
                        <div class="bundle-price"><span>$</span><?= get_sub_field( 'bundle_price' ) ?></div>
                        <div class="bundle-callout"><?= get_sub_field( 'bundle_callout' ) ?></div>
                        <div class="bundle-features"><?= get_sub_field( 'bundle_features' ) ?></div>
                        <div class="bundle-cta"><button class="btn"><?= get_sub_field( 'cta_button_text' ) ?></button></div>
                        <?php if(get_sub_field('bundle_ribbon')) : ?>
                        <div class="bundle-ribbon"><img src="/wp-content/themes/iwt-framework/images/ribbon.png"></div>
                        <?php endif ?>
                    </a>
                </div>
			<?php endwhile; ?>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="bundle-footer-text"><?= get_field( 'pricing_bundles_footer_text' ) ?></div>
            </div>
        </div>
    </section>

    <hr>

    <section class="container tools-wrapper">
        <div class="row intro">
            <div class="col-sm-12">
                <header>
                    <h2><?= get_field( 'tools_introduction' ) ?></h2>
                    <p><?= get_field( 'tools_introduction_subtext' ) ?></p>
                </header>
            </div>
        </div>
		<?php $x = 0; ?>
		<?php while ( have_rows( 'tools' ) ) : the_row(); ?>
			<?php
			/**
			 * Even numbered entries have image on the left, odd numbered entries have images on the right
			 */
			?>
            <div class="row tools">
				<?php if ( $x % 2 == 0 ) : // This is an even numbered entry, so image on left ?>
                    <div class="col-sm-6 tool-image">
                        <img src="<?= get_sub_field( 'tool_image' ) ?>" title="<?= get_sub_field( 'tool_title' ) ?>">
                    </div>
                    <div class="col-sm-6 tool-details">
                        <div class="tool-subtitle"><?= get_sub_field( 'tool_subtitle' ) ?></div>
                        <div class="tool-title"><?= get_sub_field( 'tool_title' ) ?></div>
                        <div class="tool-content"><?= get_sub_field( 'tool_content' ) ?></div>
                    </div>
				<?php else : // Odd numbered entry, image on right ?>
                    <div class="col-sm-6 tool-details">
                        <div class="tool-subtitle"><?= get_sub_field( 'tool_subtitle' ) ?></div>
                        <div class="tool-title"><?= get_sub_field( 'tool_title' ) ?></div>
                        <div class="tool-content"><?= get_sub_field( 'tool_content' ) ?></div>
                    </div>
                    <div class="col-sm-6 tool-image">
                        <img src="<?= get_sub_field( 'tool_image' ) ?>" title="<?= get_sub_field( 'tool_title' ) ?>">
                    </div>
				<?php endif ?>
            </div>
			<?php $x ++ ?>
		<?php endwhile; ?>
        <div class="row callouts">
			<?php while ( have_rows( 'callouts' ) ) : the_row(); ?>
                <div class="col-sm-4">
                    <div class="callout-image"><img src="<?= get_sub_field( 'callout_image' ) ?>"></div>
                    <div class="callout-title"><?= get_sub_field( 'callout_headline' ) ?></div>
                    <div class="callout-text"><?= get_sub_field( 'callout_text' ) ?></div>
                </div>
			<?php endwhile; ?>
        </div>
    </section>

    <hr>

    <section class="container cta">
        <div class="row">
            <div class="cta-headline h1"><?= get_field( 'cta_headline' ) ?></div>
            <div class="cta-text"><?= get_field('cta_text') ?></div>
            <div class="cta-button"><a class="btn btn-primary" href="<?= get_field('cta_button_link') ?>"><?= get_field('cta_button_text') ?></a></div>
        </div>
    </section>
</div>
