<?php
/**
 * Template for displaying contact page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_EDUT_Project
 */

?>

<?php jsd_theme_debug( pathinfo( __FILE__, PATHINFO_FILENAME ), basename( __DIR__ ) ) ?>

<div id="primary" class="content-area row">
    <main id="main" class="site-main col-sm-12" role="main">
        <article id="post-<?php the_ID(); ?>" <?php post_class("contact"); ?>>
            <header class="entry-header">
                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            </header><!-- .entry-header -->

            <div class="entry-content">
                <?php the_content(); ?>
            </div><!-- .entry-content -->

            <?php if ( get_edit_post_link() ) : ?>
                <footer class="entry-footer">
                </footer><!-- .entry-footer -->
            <?php endif; ?>
        </article><!-- #post-## -->
    </main>
</div>
