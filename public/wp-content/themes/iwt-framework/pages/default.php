<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_EDUT_Project
 */

?>

<?php jsd_theme_debug( basename( __DIR__ ), pathinfo( __FILE__, PATHINFO_FILENAME ) ) ?>

<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<?php jsd_breadcrumbs() ?>
                </header><!-- .entry-header -->
            </div>
        </div>
    </div>
</div>

<div id="primary" class="content-area container">
    <div class="row">
        <main id="main" class="site-main col-sm-8">
            <article id="post-<?php the_ID(); ?>" <?php post_class( 'default' ); ?>>
                <div class="entry-content">
					<?php the_content(); ?>
                </div><!-- .entry-content -->
            </article><!-- #post-## -->
        </main>

		<?php if ( get_field( 'display_sidebar' ) ) : ?>
			<aside id='secondary' class='widget-area col-sm-4'>
				<?php dynamic_sidebar( 'sidebar-page' ) ?>
			</aside>
		<?php endif ?>
    </div>
</div>

