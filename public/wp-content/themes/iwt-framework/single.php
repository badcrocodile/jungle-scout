<?php
/**
 * The template for displaying all single posts
 */

jsd_theme_debug( basename( __DIR__ ), pathinfo( __FILE__, PATHINFO_FILENAME ) );

get_header();

while ( have_posts() ) : the_post();
	/**
	 * This is the router for single-post requests
	 *
	 * Usage: If you want a different layout for single posts of Post Type "Books",
	 * add a is_singular check in the switch below
	 * and point it to your Books template
	 * page.php holds routing rules for pages
	 * archive.php holds routing rules for displaying archives
	 */

	switch ( true ) {
		case( is_singular( 'books' ) ):
			get_template_part( 'posts/books', 'default' );
			break;
		default:
			get_template_part( 'posts/post', 'default' );
	}

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
endwhile; // End of the loop.

get_footer();
