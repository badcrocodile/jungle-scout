<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wtf
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer">
    <div class="container">
        <div class="row">
			<aside id="footer_1" class="widget-area col-sm-3">
				<?php dynamic_sidebar( 'footer-1' ); ?>
			</aside><!-- #secondary -->
			<aside id="footer_2" class="widget-area col-sm-3">
				<?php dynamic_sidebar( 'footer-2' ); ?>
			</aside><!-- #secondary -->
			<aside id="footer_3" class="widget-area col-sm-3">
				<?php dynamic_sidebar( 'footer-3' ); ?>
			</aside><!-- #secondary -->
			<aside id="footer_4" class="widget-area col-sm-3">
				<?php dynamic_sidebar( 'footer-4' ); ?>
			</aside><!-- #secondary -->
        </div>
    </div>

    <!-- ACF json file to generate Navigation links included in theme directory -->
	<?php while ( have_rows( 'footer_links', 'option' ) ) : the_row(); ?>
        <li><a href="<?= get_sub_field( 'link_url' ) ?>" title="<?= get_sub_field( 'link_text' ) ?>"><?= get_sub_field( 'link_text' ) ?></a></li>
	<?php endwhile; ?>
</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
