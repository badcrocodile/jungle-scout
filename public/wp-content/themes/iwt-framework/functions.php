<?php
/**
 * IWT functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wtf
 */

@ini_set( 'upload_max_size', '64M' );
@ini_set( 'post_max_size', '64M' );
@ini_set( 'max_execution_time', '300' );

// Register Custom Navigation Walker
require_once get_template_directory() . '/libraries/wp-bootstrap-navwalker.php';

if ( ! function_exists( 'wtf_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wtf_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on wtf, use a find and replace
		 * to change 'wtf' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'wtf', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'wtf' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'wtf_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'wtf_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wtf_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wtf_content_width', 640 );
}

add_action( 'after_setup_theme', 'wtf_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wtf_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Home Sidebar', 'wtf' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'The homepage sidebar', 'wtf' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Page Sidebar', 'wtf' ),
		'id'            => 'sidebar-page',
		'description'   => esc_html__( 'A sidebar designed for pages', 'wtf' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Post Sidebar', 'wtf' ),
		'id'            => 'sidebar-post',
		'description'   => esc_html__( 'A sidebar designed for blog posts', 'wtf' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'wtf' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'wtf' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'wtf' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'wtf' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'wtf' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'wtf' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 4', 'wtf' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'wtf' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}

add_action( 'widgets_init', 'wtf_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'wtf_scripts' );
function wtf_scripts() {
	wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/libraries/bootstrap/css/bootstrap.min.css' );
//	wp_enqueue_style( 'swipebox-css', get_stylesheet_directory_uri() . '/libraries/swipebox/css/swipebox.min.css' );
//	wp_enqueue_style( 'flexslider-css', get_stylesheet_directory_uri() . '/libraries/flexslider/flexslider.css' );
//	wp_enqueue_style( 'fancybox-css', get_stylesheet_directory_uri() . '/libraries/fancybox/dist/jquery.fancybox.min.css' );
//	wp_enqueue_style( 'slick', get_stylesheet_directory_uri() . '/libraries/slick/slick.css' );
//	wp_enqueue_style( 'slick-style', get_stylesheet_directory_uri() . '/libraries/slick/slick-theme.css' );
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:400,700,900' );
//	wp_enqueue_style( 'fontawesome-5', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css' );
	wp_enqueue_style( 'mmenu', get_stylesheet_directory_uri() . '/libraries/mmenu/dist/jquery.mmenu.all.css' );
	wp_enqueue_style( 'hamburger', get_stylesheet_directory_uri() . '/libraries/hamburgers.min.css' );
	wp_enqueue_style( 'wtf-style', get_stylesheet_uri() );

	wp_deregister_script( 'jquery' );

	wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js', false, '2.2.0' );
	wp_enqueue_script( 'wtf-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
//	wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/libraries/bootstrap/js/bootstrap.min.js', array( 'jquery' ), '1.0' );
//	wp_enqueue_script( 'swipebox-js', get_stylesheet_directory_uri() . '/libraries/swipebox/js/jquery.swipebox.min.js', array( 'jquery' ), '1.0' );
//	wp_enqueue_script( 'flexslider-js', get_stylesheet_directory_uri() . '/libraries/flexslider/jquery.flexslider-min.js', array( 'jquery' ), '1.0' );
//	wp_enqueue_script( 'fancybox-js', get_stylesheet_directory_uri() . '/libraries/fancybox/dist/jquery.fancybox.min.js', array( 'jquery' ), '1.0' );
//	wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/libraries/slick/slick.min.js', array( 'jquery' ), '1.0' );
	wp_enqueue_script( 'mmenu', get_stylesheet_directory_uri() . '/libraries/mmenu/dist/jquery.mmenu.all.js', array( 'jquery' ), '1.0' );
	wp_enqueue_script( 'match-height', get_stylesheet_directory_uri() . '/libraries/match-height/dist/jquery.matchHeight-min.js', array( 'jquery' ), '1.0' );
	wp_enqueue_script( 'wtf-scripts', get_template_directory_uri() . '/js/scripts.js', array( 'jquery' ), '20151215', true );

	wp_enqueue_script( 'wtf-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Create custom post type
 */
// add_action('init', 'post_type_survivor', 0);
function post_type_survivor() {
	$labels = array(
		'name'                  => _x( 'Survivor Stories', 'Post Type General Name', 'edut_project' ),
		'singular_name'         => _x( 'Survivor Story', 'Post Type Singular Name', 'edut_project' ),
		'menu_name'             => __( 'Survivor Stories', 'edut_project' ),
		'name_admin_bar'        => __( 'Survivor Story', 'edut_project' ),
		'archives'              => __( 'Item Archives', 'edut_project' ),
		'attributes'            => __( 'Story Attributes', 'edut_project' ),
		'parent_item_colon'     => __( 'Parent Story:', 'edut_project' ),
		'all_items'             => __( 'All Stories', 'edut_project' ),
		'add_new_item'          => __( 'Add New Story', 'edut_project' ),
		'add_new'               => __( 'Add New', 'edut_project' ),
		'new_item'              => __( 'New Story', 'edut_project' ),
		'edit_item'             => __( 'Edit Story', 'edut_project' ),
		'update_item'           => __( 'Update Story', 'edut_project' ),
		'view_item'             => __( 'View Story', 'edut_project' ),
		'view_items'            => __( 'View Stories', 'edut_project' ),
		'search_items'          => __( 'Search Story', 'edut_project' ),
		'not_found'             => __( 'Not found', 'edut_project' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'edut_project' ),
		'featured_image'        => __( 'Featured Image', 'edut_project' ),
		'set_featured_image'    => __( 'Set featured image', 'edut_project' ),
		'remove_featured_image' => __( 'Remove featured image', 'edut_project' ),
		'use_featured_image'    => __( 'Use as featured image', 'edut_project' ),
		'insert_into_item'      => __( 'Insert into story', 'edut_project' ),
		'uploaded_to_this_item' => __( 'Uploaded to this story', 'edut_project' ),
		'items_list'            => __( 'Stories list', 'edut_project' ),
		'items_list_navigation' => __( 'Stories list navigation', 'edut_project' ),
		'filter_items_list'     => __( 'Filter stories list', 'edut_project' ),
	);
	$args   = array(
		'label'               => __( 'Survivor Story', 'edut_project' ),
		'description'         => __( 'Survivor Stories', 'edut_project' ),
		'labels'              => $labels,
		'supports'            => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			'comments',
			'trackbacks',
			'revisions',
			'custom-fields',
			'page-attributes',
			'post-formats',
		),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-id-alt',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite'             => array( 'slug' => 'survivor' ),
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'survivor_story', $args );

}

add_action( 'login_enqueue_scripts', 'wtf_login_logo_one' );
function wtf_login_logo_one() {
	?>
    <style type="text/css">
        body.login {
            background: #fff !important;
        }

        body.login div#login h1 a {
            background-image: url(/wp-content/themes/breaking/images/New_BB_Masthead.jpg);
            background-size: 310px;
            width: 100%;
            height: 120px;
        }

        body.login form {
            padding: 30px 20px;
            box-shadow: 0px 4px 7px 1px rgba(0, 0, 0, .13);
        }

        body.login #backtoblog {
            display: none;
        }
    </style>
	<?php
}

add_filter( 'login_headerurl', 'wtf_login_logo_url' );
function wtf_login_logo_url() {
	return home_url();
}

add_filter( 'login_headertitle', 'wtf_login_logo_url_title' );
function wtf_login_logo_url_title() {
	return 'WT&F Theme';
}

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( array(
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Settings',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'   => false
	) );

	acf_add_options_sub_page( array(
		'page_title'  => 'Header Settings',
		'menu_title'  => 'Header',
		'parent_slug' => 'theme-general-settings',
	) );

	acf_add_options_sub_page( array(
		'page_title'  => 'Footer Settings',
		'menu_title'  => 'Footer',
		'parent_slug' => 'theme-general-settings',
	) );
}

/**
 * Outputs information about the template being displayed.
 * Useful for debugging template files
 * Toggled on/off by ACF field in Theme Settings --> General
 *
 * @param $filepath
 * @param $filename
 */
function jsd_theme_debug( $filepath, $filename ) {
	if ( get_field( 'theme_debug', 'option' ) ) : ?>
        <script type="text/javascript">
            console.log("<?= $filepath ?>/<?= $filename ?>");
        </script><?php
	endif;
}

function jsd_breadcrumbs() {
	$showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$delimiter   = '&raquo;'; // delimiter between crumbs
	$home        = 'Home'; // text for the 'Home' link
	$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
	$before      = '<span class="current">'; // tag before the current crumb
	$after       = '</span>'; // tag after the current crumb

	global $post;
	$homeLink = get_bloginfo( 'url' );

	if ( is_home() || is_front_page() ) {

		if ( $showOnHome == 1 ) {
			echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
		}

	} else {

		echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';

		if ( is_category() ) {
			$thisCat = get_category( get_query_var( 'cat' ), false );
			if ( $thisCat->parent != 0 ) {
				echo get_category_parents( $thisCat->parent, true, ' ' . $delimiter . ' ' );
			}
			echo $before . 'Archive by category "' . single_cat_title( '', false ) . '"' . $after;

		} elseif ( is_search() ) {
			echo $before . 'Search results for "' . get_search_query() . '"' . $after;

		} elseif ( is_day() ) {
			echo '<a href="' . get_year_link( get_the_time( 'Y' ) ) . '">' . get_the_time( 'Y' ) . '</a> ' . $delimiter . ' ';
			echo '<a href="' . get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) . '">' . get_the_time( 'F' ) . '</a> ' . $delimiter . ' ';
			echo $before . get_the_time( 'd' ) . $after;

		} elseif ( is_month() ) {
			echo '<a href="' . get_year_link( get_the_time( 'Y' ) ) . '">' . get_the_time( 'Y' ) . '</a> ' . $delimiter . ' ';
			echo $before . get_the_time( 'F' ) . $after;

		} elseif ( is_year() ) {
			echo $before . get_the_time( 'Y' ) . $after;

		} elseif ( is_single() && ! is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object( get_post_type() );
				$slug      = $post_type->rewrite;
				echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
				if ( $showCurrent == 1 ) {
					echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
				}
			} else {
				$cat  = get_the_category();
				$cat  = $cat[0];
				$cats = get_category_parents( $cat, true, ' ' . $delimiter . ' ' );
				if ( $showCurrent == 0 ) {
					$cats = preg_replace( "#^(.+)\s$delimiter\s$#", "$1", $cats );
				}
				echo $cats;
				if ( $showCurrent == 1 ) {
					echo $before . get_the_title() . $after;
				}
			}

		} elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' && ! is_404() ) {
			$post_type = get_post_type_object( get_post_type() );
			echo $before . $post_type->labels->singular_name . $after;

		} elseif ( is_attachment() ) {
			$parent = get_post( $post->post_parent );
			$cat    = get_the_category( $parent->ID );
			$cat    = $cat[0];
			echo get_category_parents( $cat, true, ' ' . $delimiter . ' ' );
			echo '<a href="' . get_permalink( $parent ) . '">' . $parent->post_title . '</a>';
			if ( $showCurrent == 1 ) {
				echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
			}

		} elseif ( is_page() && ! $post->post_parent ) {
			if ( $showCurrent == 1 ) {
				echo $before . get_the_title() . $after;
			}

		} elseif ( is_page() && $post->post_parent ) {
			$parent_id   = $post->post_parent;
			$breadcrumbs = array();
			while ( $parent_id ) {
				$page          = get_page( $parent_id );
				$breadcrumbs[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
				$parent_id     = $page->post_parent;
			}
			$breadcrumbs = array_reverse( $breadcrumbs );
			for ( $i = 0; $i < count( $breadcrumbs ); $i ++ ) {
				echo $breadcrumbs[ $i ];
				if ( $i != count( $breadcrumbs ) - 1 ) {
					echo ' ' . $delimiter . ' ';
				}
			}
			if ( $showCurrent == 1 ) {
				echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
			}

		} elseif ( is_tag() ) {
			echo $before . 'Posts tagged "' . single_tag_title( '', false ) . '"' . $after;

		} elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata( $author );
			echo $before . 'Articles posted by ' . $userdata->display_name . $after;

		} elseif ( is_404() ) {
			echo $before . 'Error 404' . $after;
		}

		if ( get_query_var( 'paged' ) ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
				echo ' (';
			}
			echo __( 'Page' ) . ' ' . get_query_var( 'paged' );
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
				echo ')';
			}
		}

		echo '</div>';

	}
}

/**
 * Tell ACF to not hide regular WP custom fields
 */
add_filter( 'acf/settings/remove_wp_meta_box', '__return_false' );